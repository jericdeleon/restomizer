# Restomizer
Can't decide where to eat? Randomize it!

This application is built using Nativescript (Angular2), with Yelp's Fusion API.

---
# Hardware Prerequisites

### Windows
Only Genymotion has hard requirements for this application's tech stack.
Check the req's [here](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm).

### OS X
XCode 8.3 can only be installed on OSX 10.12 (Sierra) or later.
Check your Macbook model for compatibility.

---
# Software Prerequisites
The following software needs to be installed on any dev machine:

### OS X
1. **MAKE SURE** to sign in to iCloud using your own account.
- Install [Homebrew](http://brew.sh/)
- Install Homebrew Cask:
    - `brew tap caskroom/cask`
- *(Optional)* [Enhance your terminal](https://gist.github.com/kevin-smets/8568070)
- [Install rbenv](https://github.com/rbenv/rbenv)
    - `brew install rbenv`; don't forget to follow the caveat and run `rbenv init`
    - `rbenv install <version>`: install latest Ruby LTS (currently 2.3.*)
    - `rbenv global <version>`: set the global Ruby version to the downloaded LTS
- Install required gems:
    - `sudo gem install xcodeproj`
    - `sudo gem install cocoapods`
- Install Java:
    - `brew cask install java`
    - 'brew install jenv'
    - `brew tap caskroom/versions`
    - `jenv add <javapath>`: usually: /Library/Java/JavaVirtualMachines/1.*.0.jdk/Contents/Home
    - `jenv global <version>`
    - `jenv enable-plugin export`
    - `jenv enable-plugin maven`
- Install Android-SDK:
    - `brew install android-sdk`; don't forget to export ANDROID_HOME as stated in the caveats
    - `android sdk`: make sure that the ff. packages are installed:
        - `Tools/Android SDK Tools`
        - `Tools/Android SDK Platform Tools`
        - `Tools/Android SDK Build-tools` (install the highest version of each Major API version)
        - (Major API version, API 22 to highest)`/SDK Platform`
        - (Major API version, API 22 to highest)`/Sources for Android SDK`
        - `Extras/*` (install all)
- [Install Virtualbox](https://www.virtualbox.org/wiki/Downloads)
    - follow the recommended version by Genymotion
    - restart the machine
- [Genymotion](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm)
    - open `Genymotion` > `Settings` > `ADB`, use `Custom Android SDK`, and put your `ANDROID_HOME` path
    - add any device that you wish to emulate
- Install NVM:
    - `brew install nvm`; don't forget to add the source lines in the Caveat section
    - `nvm ls-remote --lts`: list latest recommended Nativescript version
    - `nvm install <version>`
- Install Nativescript
    - `npm install -g nativescript`
    - `tns doctor`
    
    
### Windows
1. Install [Chocolatey](https://chocolatey.org/install)
    - open Powershell with Admin rights
    - enable Remote Script Install: `Set-ExecutionPolicy RemoteSigned`
    - run the install script (details in website)
- Install all tools:
    - `choco install -y git`
    - `choco install -y cmder`
        - always use Admin Rights, with profile: `bash::bash as Admin`
    - `choco install -y visualstudiocode`
        - after installation, add the [Nativescript VS Code Extension](https://www.nativescript.org/nativescript-for-visual-studio-code)
    - `choco install -y jdk8`
    - `choco install -y android-sdk`
        - run the SDK Manager: `android`
            - might not work in CMDer; try in CMD instead
        - execute SDK Manager as Administrator, and make sure that the ff. packages are installed:
            - `Tools/Android SDK Tools`
            - `Tools/Android SDK Platform Tools`
            - `Tools/Android SDK Build-tools/*`
            - (Major API version, API 21 to highest)`/SDK Platform`
            - `Extras/*` (install all applicable)
- [Genymotion](https://docs.genymotion.com/Content/01_Get_Started/Installation.htm)
    - open `Genymotion` > `Settings` > `ADB`, use `Custom Android SDK`, and put your `ANDROID_HOME` path
    - add any device that you wish to emulate
    - for each device, install/update Google Play (for Google Maps SDK): follow the guide [here](https://github.com/codepath/android_guides/wiki/Genymotion-2.0-Emulators-with-Google-Play-support#setup-google-play-services)
- [NVM for Windows](https://github.com/coreybutler/nvm-windows)
    - `choco install -y nvm`
    - afterwards, install the **latest** Node LTS version:
        - `nvm list available`; take note of the latest LTS version; ex: 6.9.5
        - `nvm install 6.9.5 <arch>` (default: 64-bit; example for 32-bit: nvm install 6.9.5 32)
        - `nvm list` (Node 6.9.5 must be available)
        - `nvm use <version>`
    - check by executing: `npm version` & `node --version`
    - install Nativescript npm module: `npm i -g nativescript`
        - check by executing: `tns doctor`

Your system should now be ready to build the application.

---
# Dev Workflow
1. Clone the project
- Open the restomizer folder in VS Code (if debugging, open with Admin rights)
- Open the restomizer folder in the CLI with Admin rights: `cd restomizer`
- Install Node dependencies and Nativescript plugins: `tns install`
- Build the application: `tns build android --clean`
- Make sure to that your local build is pointing to the correct environment (See `Environments` section below)
- Start an Android emulator device, or connect your physical Android device, and check if connected: `tns device`
    - Run the application on the emulator: `tns run android --emulator`
    - Run the application on the device: `tns run android --device <device id>`
- To debug / inspect the view tree, run: `tns debug android --start --emulator`
- To use webpack / create a bundled .apk (smaller/faster):
    -  Delete build folders 
    - `npm install`
    - `npm run start-android-bundle`
        - (As of NS 3.1, applicable only for OS X / Linux) To package a snapshot as well (even faster startup): `npm run start-android-bundle --snapshot`

### Additional Notes:
- If a new plugin was introduced (or you're encountering weird build errors) and you wish to rebuild the everything from scratch:
    1. Delete these folders:
        - hooks
        - node_modules
        - lib
        - platforms
    - Redo `Dev Workflow` steps starting from Step 4.


---
# Environments
For the backend APIs, there will be 3 environments: DEV, UAT and PROD
- `DEV`: for local testing
- `UAT`: for client testing
- `PROD`: for users 

*to follow*

### Switching Environments
*to follow*

---
# Guides
If you are unaware of certain tech, you can find basic information/commands in the guides below:
- Basics
    - [Git](http://rogerdudler.github.io/git-guide/)
        - useful commands:
            - prune all stale remotes: `git remote prune origin`
            - stash current modified/untracked files: `git stash`
            - recover stash: `git stash apply --index`
    - [Basic Git Workflow](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)
    - [Markdown](http://commonmark.org/help/)
    - [TypeScript: Deep Dive](https://www.gitbook.com/book/basarat/typescript)
- Angular
    - [Component Interaction](https://angular.io/docs/ts/latest/cookbook/component-communication.html)
    - [Providers](https://www.sitepoint.com/angular-2-components-providers-classes-factories-values/)
- Nativescript
    - [Nativescript CLI](https://github.com/NativeScript/nativescript-cli)
    - [Nativescript API](http://docs.nativescript.org/api-reference/globals)
    - [Nativescript Plugin: Google Maps SDK](https://github.com/dapriett/nativescript-google-maps-sdk)
    - [Nativescript Plugin: Firebase](https://github.com/EddyVerbruggen/nativescript-plugin-firebase)
    - Nativescript UI / UX:
        - from Nativescript Docs:
            - [Working with Images](https://docs.nativescript.org/ui/images#load-images-from-a-resource)
            - [Supporting Multiple Screens](http://docs.nativescript.org/ui/supporting-multiple-screens)
        - from Nativescript Blog:
            - [Supporting Multiple Screens](https://www.nativescript.org/blog/supporting-multiple-screen-resolutions-in-your-nativescript-app)
        - Tools:
            - [Nativescript Image Builder](http://nsimage.nativescript.rocks/)
    - Nativescript Angular
        - [Lifecycle Hook diffs bet. Web And {N}](https://github.com/NativeScript/nativescript-angular/issues/374)
- Publishing
    - Android
        - [Android] (https://docs.nativescript.org/publishing/publishing-android-apps)
    - iOS
        - [Process Checklist] (https://docs.nativescript.org/publishing/publishing-ios-apps)
        - [Provisioning Profile Guide](https://wiki.appcelerator.org/display/guides2/Deploying+to+iOS+devices)


            