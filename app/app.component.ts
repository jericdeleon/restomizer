// Core
import { Component } from "@angular/core";
import {Page} from "ui/page";

// Nativescript FontIcon
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
@Component({
    selector: "rm-app",
    templateUrl: "app.component.html",
})

export class AppComponent {
    constructor(
        private page: Page,
        private fonticon: TNSFontIconService
    ) {
        // this.page.actionBarHidden = true;
    }
}
