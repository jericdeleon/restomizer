// Core
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";

// Modules
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";

// Components
import { AppComponent } from "./app.component";
import { navigatableComponents, entryComponents } from "./app.routing";

// Services
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { BusinessService } from "./services/business.service";
import { DataService } from "./services/data.service";

// Google Maps
import * as platform from "platform";
declare var GMSServices: any;
if (platform.isIOS) { 
    GMSServices.provideAPIKey("PUT_API_KEY_HERE");
}

// Nativescript Font Icons
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpModule,
		TNSFontIconModule.forRoot({
			'fa': './assets/font-awesome.css'
		})
    ],
    declarations: [
        AppComponent,
        ...navigatableComponents,
        ...entryComponents
    ],
    entryComponents: [
        ...entryComponents
    ],
    providers: [
        ModalDialogService,
        BusinessService,
        DataService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
