import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

// Components
import { BusinessesComponent } from "./components/businesses/businesses.component";

// Modals
import { SearchOptionModal } from "./components/businesses/modals/search-option/search-option.modal";

const routes: Routes = [
    { path: "", redirectTo: "/businesses", pathMatch: "full" },
    { path: "businesses", component: BusinessesComponent },
];

export const navigatableComponents = [
    BusinessesComponent
];

export const entryComponents = [
    SearchOptionModal
]

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }