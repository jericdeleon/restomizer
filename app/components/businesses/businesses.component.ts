// Core
import { Component, OnInit, ViewContainerRef } from "@angular/core";

// Modals
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { SearchOptionModal } from "./modals/search-option/search-option.modal"

// Models
import { Business } from "../../models/business";

// Services
import { BusinessService } from "../../services/business.service";
import { DataService } from "../../services/data.service";

// Utility
import { Observable, Subject, BehaviorSubject } from "rxjs/Rx";
import "rxjs/add/observable/fromEvent";

// Loading Icon
const LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;

// Permissions
const permissions = require( "nativescript-permissions" );

// Geolocation
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums";
const geolocationOptions = {
    desiredAccuracy: Accuracy.high, 
    maximumAge: 5000, 
    timeout: 20000 
}

// Google Maps
import * as platform from "platform";
import { MapView, Position, Marker, Style } from "nativescript-google-maps-sdk";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

// Directions
import { Directions } from "nativescript-directions";

// Browser
import { openUrl } from "utils/utils";

// Modal
import * as dialogs from "ui/dialogs";

// Exit
import { exit } from 'nativescript-exit';

// Workaround: Android
declare var android: any;

@Component({
    moduleId: module.id,
    selector: "rm-businesses",
    templateUrl: "./businesses.component.html",
    styleUrls: ["businesses-common.css", "businesses.css"],
})
export class BusinessesComponent implements OnInit {
    // State streams
    businesses$: BehaviorSubject<Business[]>;

    searchOptionsChange$: BehaviorSubject<string>;
    currentSearchOption: string;

    // State
    business: Business;

    // Loader
    loader: any;

    // Map
    mapView: MapView;

    constructor(
        private modal: ModalDialogService, 
        private vcRef: ViewContainerRef,
        private businessService: BusinessService,
        private dataService: DataService,
    ) { 
        this.businesses$ = new BehaviorSubject<Business[]>([]);
        this.searchOptionsChange$ = new BehaviorSubject<string>(JSON.stringify(this.dataService.searchOptions));
        this.currentSearchOption = "";
        
        this.business = new Business();

        this.loader = new LoadingIndicator();
    }
    
    ngOnInit(): void {
        // map init: onMapReady();

        
    }

    onMapReady(args): void {
        // setup: map
        this.mapView = args.object;
        this.mapView.zoom = 14;

        // first, ask location permission
        this.ensureLocationIsEnabled()
            .catch(result => {
                // Location request was denied; exit the application
                console.log("geolocation.enableLocationRequest() REJECTED");
                exit();
            })
            .then(result => {
                // setup: map settings, permission-dependent, platform-specific

                if (platform.isAndroid) {
                    // show:  current location, location button
                    let uiSettings = this.mapView.gMap.getUiSettings();
                    uiSettings.setMyLocationButtonEnabled(true);
                    this.mapView.gMap.setMyLocationEnabled(true);
                } else if (platform.isIOS) {
                    // show:  current location, location button
                    this.mapView.gMap.myLocationEnabled = true;
                    this.mapView.gMap.settings.myLocationButton = true;
                }

                // current location is ready; center Map to current location
                return this.centerMapToCurrentLocation(this.mapView);
            })
            .then(result => {
                // get business data

                // ensure access token is ready; block UI; show loading icon
                this.loader.show();
        
                // wait for access token to get ready before enabling UI
                this.businessService.accessTokenSubject
                    .subscribe({
                        next: (value) => {},
                        complete: () => {
                            this.checkSearchOptionAndRandomize()
                                .then(result => {
                                    this.loader.hide();
                                });
                        }
                    });
            });
    }

    // UI button: open search modal
    openSearchOptions() {
        let options = {
            context: {},
            fullscreen: false,
            viewContainerRef: this.vcRef
        };

        this.modal.showModal(SearchOptionModal, options).then(res => {
            this.searchOptionsChange$.next(JSON.stringify(res));
        });
    }

    // UI button: randomize
    checkSearchOptionAndRandomize(): Promise<void> {
        return Promise.resolve()
            .then(result => {
                // console.log("soc: " + JSON.stringify(this.searchOptionsChange$.value));
                // console.log("cso: "+ this.currentSearchOption);

                // first, check if currentSearchOption is different from the modal's return value
                if (JSON.stringify(this.searchOptionsChange$.value) != this.currentSearchOption) {
                    console.log("Businesses will be updated!");

                    // if searchOption was updated, get new businesses
                    this.currentSearchOption = JSON.stringify(this.searchOptionsChange$.value);

                     // block UI
                    this.loader.show();

                    return geolocation.getCurrentLocation(geolocationOptions)
                        .then(result => {
                            return this.businessService.getBusinesses(result.latitude, result.longitude, JSON.parse(this.searchOptionsChange$.value))
                        })    
                        .then(businesses => {
                            // setup: businesses / business
                            this.businesses$.next(businesses);

                            // hide loading icon
                            this.loader.hide();

                            return Promise.resolve();
                        });
                } else {
                    console.log("Businesses will NOT be updated!");

                    // searchOption unchanged;
                    return Promise.resolve();
                }        
            })
            .then(result => {
                // update state: business
                const randomBusiness = this.getRandomBusiness();
                const newBusiness = randomBusiness != null ? randomBusiness : new Business();

                // set new business
                this.business = newBusiness;

                // create marker and center camera
                if (newBusiness.coordinates != null) {
                    this.createMarkerAndCenterMapToMarker(this.mapView, newBusiness);
                }
            });
    }

    // UI button: center map to current business
    centerToCurrentBusiness(): void {
        this.centerMapToLocation(this.mapView, this.business.coordinates.latitude, this.business.coordinates.longitude);
    }

    // UI: open in browser
    openInBrowser(): void {
        openUrl(this.business.url);
    }
    // UI button: open default Maps app to show directions
    openDirections(): void {
        const directions = new Directions();

        // first, check if phone has any default Map app
        directions.available()
            .then(avail => {
                if (avail) {
                    // get current location

                    return geolocation.getCurrentLocation(geolocationOptions)
                        .then(result => {
                            return directions.navigate({
                                    from: { // optional, default 'current location'
                                        lat: result.latitude,
                                        lng: result.longitude
                                    },
                                    to: { // if an Array is passed (as in this example), the last item is the destination, the addresses in between are 'waypoints'.
                                        lat: this.business.coordinates.latitude,
                                        lng: this.business.coordinates.longitude
                                    },
                                    ios: {
                                        preferGoogleMaps: true, // If the Google Maps app is installed, use that one instead of Apple Maps, because it supports waypoints. Default true.
                                        allowGoogleMapsWeb: true // If waypoints are passed in and Google Maps is not installed, you can either open Apple Maps and the first waypoint is used as the to-address (the rest is ignored), or you can open Google Maps on web so all waypoints are shown (set this property to true). Default false.
                                    }
                                });
                        });
                } else {
                    return dialogs.alert("You have no default Map application");
                }
            });
    }

    private createMarkerAndCenterMapToMarker(mapView: MapView, business: Business): Promise<void> {
        // remove all markers, 
        mapView.removeAllMarkers();

        // create new Marker
        const marker = new Marker();
        marker.position = Position.positionFromLatLng(business.coordinates.latitude, business.coordinates.longitude);
        marker.title = business.name;
        marker.snippet = business.rating.toString();
        
        // Add marker to map
        mapView.addMarker(marker);

        // center map to marker
        return this.centerMapToLocation(mapView, business.coordinates.latitude, business.coordinates.longitude);
    }

    private centerMapToCurrentLocation(mapView: MapView): Promise<void> {
        return geolocation.getCurrentLocation(geolocationOptions)
            .then(result => {
                console.log("Current Location: " + result.latitude + ":" + result.longitude);
                return this.centerMapToLocation(mapView, result.latitude, result.longitude);
            });
    }

    private centerMapToLocation(mapView: MapView, lat: number, lng: number): Promise<void> {
        mapView.latitude = lat;
        mapView.longitude = lng;
        return Promise.resolve();
    }
    
    private ensureLocationIsEnabled(): Promise<void> {

        if (platform.isIOS) {
            // If iOS, resolve
            return Promise.resolve();
        } else if (platform.isAndroid) {
            // If Android

            // first, ask for app's location permissions (specific to Android 6+ / API 23+)
            return permissions.requestPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION)
                .then(result => {
                    // second, ask for location services
                    if (geolocation.isEnabled()) {
                        console.log("Geolocation IS enabled!");

                        return Promise.resolve();
                    } else {
                        console.log("Geolocation IS NOT enabled!");

                        return dialogs.confirm("Restomizer needs your current location. Please enable your phone's Location service.")
                            .then(result => {
                                if (!result) {
                                    return Promise.reject(null);
                                };
                            })
                            .then(result => {
                                return geolocation.enableLocationRequest(true);
                            });
                    }  
                })
        }
    }
    
    private getRandomBusiness(): Business {
        let max = this.businesses$.value.length;
        let randomInt = Math.floor(Math.random() * (this.businesses$.value.length));

        // console.log("randomized: " + randomInt + "/" + max);

        return this.businesses$.value[randomInt];
    }

    // UI button: test
    clearCache(): void {
        this.businessService.clearCache();
    }

    // UI helper: get categories
    getCategories(): string {
        if (this.business.categories == null) {
            return ""
        } else {
            return this.business.categories
                .map(category => category.title)
                .toString()
                .split(",")
                .join(", ");
        }
    }
}