// Core
import { Component } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";

// Models
import { SearchOptionsLocalStorage } from "../../../../models/searchOptions";

// Services
import { DataService } from "../../../../services/data.service";
 
@Component({
    moduleId: module.id,
    selector: "rm-search-option",
    templateUrl: "./search-option.modal.html",
})
export class SearchOptionModal {
    // State 
    searchOptions: SearchOptionsLocalStorage;
 
    constructor(
        private params: ModalDialogParams,
        private dataService: DataService) {
        this.searchOptions = dataService.searchOptions;
    }
 
    // UI button
    close() {
        this.params.closeCallback(this.searchOptions);
    }
 
}