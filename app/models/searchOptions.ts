

export interface SearchOptions {
    price1: boolean;
    price2: boolean;
    price3: boolean;
    price4: boolean;
    radius: number;
}

/**
 * SearchOptions implementation, backed by Local Storage.
 * Fields in Storage are appended with "so_" (ex: so_price, etc.)
 */
export class SearchOptionsLocalStorage implements SearchOptions {
    // price
    private _price1: boolean;
    get price1(): boolean {
        this._price1 = (localStorage.getItem("so_price1") == "true");
        return this._price1;
    }
    set price1(v: boolean) {
        this._price1 = v;
        localStorage.setItem("so_price1", v.toString());
    } 

    // price2
    private _price2: boolean;
    get price2(): boolean {
        this._price2 = (localStorage.getItem("so_price2") == "true");
        return this._price2;
    }
    set price2(v: boolean) {
        this._price2 = v;
        localStorage.setItem("so_price2", v.toString());
    } 

    // price3
    private _price3: boolean;
    get price3(): boolean {
        this._price3 = (localStorage.getItem("so_price3") == "true");
        return this._price3;
    }
    set price3(v: boolean) {
        this._price3 = v;
        localStorage.setItem("so_price3", v.toString());
    } 

    // price4
    private _price4: boolean;
    get price4(): boolean {
        this._price4 = (localStorage.getItem("so_price4") == "true");
        return this._price4;
    }
    set price4(v: boolean) {
        this._price4 = v;
        localStorage.setItem("so_price4", v.toString());
    } 

    // radius
    private _radius: number;
    get radius(): number {
        this._radius =  +localStorage.getItem("so_radius");
        return this._radius;
    }
    set radius(v: number) {
        this._radius = v;
        localStorage.setItem("so_radius", v.toString());
    } 

    constructor() {
        // init: local storage
        require("nativescript-localstorage");

        this.price1 = (localStorage.getItem("so_price1") == "true");
        this.price2 = (localStorage.getItem("so_price2") == "true");
        this.price3 = (localStorage.getItem("so_price3") == "true");
        this.price4 = (localStorage.getItem("so_price4") == "true");
        this.radius = +localStorage.getItem("so_radius");
    }
}


