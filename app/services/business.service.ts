// Angular
import { Injectable } from "@angular/core";
import { Http, Request, RequestMethod, RequestOptions, Response, Headers } from "@angular/http";

// RxJS
import { Observable, BehaviorSubject } from "rxjs/Rx";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/toPromise';

// Models
import { SearchReponse, Business } from "../models/business";
import { SearchOptions } from "../models/searchOptions";

// Config
import { config } from "../config";

// Cache
require( "nativescript-localstorage" );

// Utility Classes
class AccessToken {
    access_token: string;
    expires_in: number;
    token_type: string;
}

@Injectable()
export class BusinessService {
    // State
    accessTokenSubject: BehaviorSubject<any>;

    // Http
    getHeader: Headers;

    // Client Cache
    businesses: Business[];

    constructor(
        private http: Http) {

        // setup accessTokenSubject
        this.accessTokenSubject = new BehaviorSubject(null);
        this.accessTokenSubject
            .filter((item) => item != null);

        // when accessTokenSubject emits a value, put value in accessToken
        this.accessTokenSubject
            .subscribe({
                next: (result) => {
                    console.log("Token obtained: " + this.accessTokenSubject.value); 
                },
                complete: () => {
                    console.log("accessTokenSubject completed!");
                }
            });

        // when access token arrives, pass through accessTokenSubject
        this.getAccessToken()
            .then(result => {
                this.accessTokenSubject.next(result);

                this.getHeader.append("Authorization", "Bearer " + result);

                this.accessTokenSubject.complete();
            });

        // setup getHeader
        this.getHeader = new Headers();
        this.getHeader.append("Content-Type", "application/x-www-form-urlencoded");
    }
    
    getBusinesses(lat: number, long: number, so): Promise<Business[]> {
        let priceString = "" +
            (so._price1 == true ? "1" : "") + 
            (so._price2 == true ? "2" : "") + 
            (so._price3 == true ? "3" : "") + 
            (so._price4 == true ? "4" : "")

        let priceStringFormatted = priceString.split("").join(",");

        return this.http.get(config.yelp_fusion_url_search, {
                params: {
                    //location: "Manila",
                    latitude: lat,
                    longitude: long,
                    price: priceStringFormatted,
                    radius: 40000,
                    limit: 50
                },
                headers: this.getHeader
            })
            .map(result => result.json())
            .toPromise()
            .then((response: SearchReponse) => {
                this.businesses = response.businesses;
                return Promise.resolve(response.businesses);
            });
    }

    getBusiness(id: string): Promise<Business> {
        return Promise.resolve(this.businesses.find((item) => { return item.id == id}));
    }

    // test
    clearCache(): void {
        localStorage.clear();
        console.log("Cache cleared!");
    }

    private getAccessToken(): Promise<string> {
        return this.isTokenMissingOrExpired()
            .then(isTokenExpired => {
                if (isTokenExpired) {
                    // get new access token
                    return this.getNewAccessToken();
                }
            }).then(result => {
                return Promise.resolve(localStorage.getItem("access_token"));
            });
    }

    private getNewAccessToken(): Promise<boolean> {
        return this.http.post(config.yelp_fusion_url_auth, {}, {
                params: {
                    grant_type: "client_credentials",
                    client_id: config.client_id,
                    client_secret: config.client_secret,
                }
            })
            .map(result => result.json())
            .toPromise()
            .then((result: AccessToken) => {
                localStorage.setItem("access_token", result.access_token);
                localStorage.setItem("expires_in", result.expires_in.toString());
                localStorage.setItem("cached_on", (new Date()).toJSON());
                return Promise.resolve(true);
            });
    }

    private isTokenMissingOrExpired(): Promise<boolean> {
        let expiryInCache: number = +localStorage.getItem("expires_in");

        if ( expiryInCache <= 0) {
            // nothing in cache
            return Promise.resolve(true);
        } else {
            // check if cached content is expired
            let expiryDate: Date = new Date(localStorage.getItem("cached_on"));
            let currentDate: Date = new Date();
            expiryDate.setSeconds(expiryDate.getSeconds() + expiryInCache);

            console.log("Comparing - expiryDate: " + expiryDate.toDateString());
            console.log("Comparing - currentDate: " + currentDate.toDateString());
            
            if (expiryDate > currentDate) {
                // cached content is still valid
                return Promise.resolve(false);
            } else {
                // cached content is expired; get new token
                return Promise.resolve(true);
            }
        }
    }
}