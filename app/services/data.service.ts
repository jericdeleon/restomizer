// Core
import { Injectable } from "@angular/core";

// Models
import { SearchOptionsLocalStorage } from "../models/searchOptions"; 
 
@Injectable()
export class DataService {

    searchOptions: SearchOptionsLocalStorage;
    
    constructor() {
        this.searchOptions = new SearchOptionsLocalStorage();
    }
}