/********

updateCameraAndMarker:
mapReady
businessSubject: BehaviorSubject

businessSubject			----------a--------b---------c----------d------
mapReady				--------------1--|
updateCameraAndMarker	--------------a----b---------c----------d------


businessSubject			----------a--------b---------c----------d------
mapReady				-----1--|
updateCameraAndMarker	----------a--------b---------c----------d------




var mapReadySubject = Observable.Subject();
var businessSubject =

var updateMarker = concat(mapReadySubject, businessSubject).filter()	// ignore items from mapReadySubject
updateMarker.subscribe()

********/

const Rx = require('rxjs/Rx');

const businessSubject = new Rx.BehaviorSubject(null);
const mapReadySubject = new Rx.Subject();
var updateCameraAndMarker;

var bc = 1;

// test: businessSubject observer
businessSubject.subscribe({
    next: (v) => console.log("businessSubject emitted: " + v),
});

mapReadySubject.subscribe({
    next: (v) => console.log("mapReadySubject emitted: " + v),
});

// emulate businessSubject activity
setInterval(() => {
    businessSubject.next(bc++);
}, 1000);

// emulate mapReadySubject activity
setTimeout(() => {
    mapReadySubject.next(null);
}, 3500);

setTimeout(() => {
    mapReadySubject.complete();
}, 5500);

updateCameraAndMarker = Rx.Observable.concat(mapReadySubject, businessSubject)
    .filter((v) => v != null);

updateCameraAndMarker.subscribe({
    next: (v) => console.log("updateCameraAndMarker emitted: " + v)
});
